
# Kubernetes Objects

[TOC]

## Sinn und Zweck von Objects

In Kubernetes dienen Objekte einem zentralen Zweck: sie ermöglichen die **deklarative Beschreibung und Verwaltung** von Anwendungen und Infrastruktur in einem Cluster.

Diese Objekte umfassen verschiedene Komponenten wie Pods, ReplicaSets, Deployments, Services und mehr. Sie ermöglichen es, Anwendungen bereitzustellen, zu skalieren, zu aktualisieren und fehlerfrei zu halten.

## Object Icons / Darstellung
Kubernetes-Objects werden oft grafisch mit Hilfe von Diagrammen oder Visualisierungen dargestellt

| **14 von insgesamt über 40 Objects** | 
| :---: | 
| <img src="./images/01_objects_selected.png" width="800"/> | 
| - | 

## Object Groups
Zu Beginn gab es noch nicht so viele Objects. Mit der Zeit wurden es allerdings immer mehr. Es brauchte deshalb schon bald eine bessere Struktur. Bereits in der frühen Entwicklungsphase haben sich die Entwickler deshalb darauf geeinigt, sämtliche Objects sogenannten **Object Groups** zuzuteilen. 

Klassische Objects der ersten Stunde waren der **Pod**, das **Deployment** oder **ReplicaSets**. Diese werden wir später mit Hands-on Übungen noch etwas genauer unter die Lupe nehmen. Bei der Einführun von Object-Groups, wurden diese "älteren" Objects einfachheitshalber der Gruppe **Core** zugeteilt. 

### Objects der ersten Stunde
| Pod | Deployment | ReplicaSet |
| :---: | :---: |:---: |
| <img src="./images/02_pod_270p.png" width="100"/> | <img src="./images/03_deployment_270p.png" width="100"/> | <img src="./images/04_replicaset_270p.png" width="100"/> |
| Core-Object | Apps-Object | Apps-Object |

Es gibt insgesamt 8 Hauptgruppen von Kubernetes-Objects.
Die Gruppen sind so organisiert, dass ähnliche Funktionen gebündelt werden. Hier eine Zusammenfassung der wichtigsten Kubernetes Object-Groups:
- Core Group: Diese Gruppe umfasst grundlegende Objekte wie Pods, Services und Namespaces. Sie bilden die Basis für andere Kubernetes-Ressourcen.
- Apps Group: Diese Gruppe enthält Objekte, die sich mit Anwendungsworkloads befassen, darunter Deployments, ReplicaSets und StatefulSets. Sie ermöglichen das Skalieren, Aktualisieren und Verwalten von Anwendungen.
- Batch Group: Diese Gruppe beinhaltet Objekte für die Ausführung von Batch-Jobs, wie z.B. CronJobs. Sie ermöglichen die geplante und wiederholte Ausführung von Aufgaben.
- Storage Group: Diese Gruppe umfasst Objekte zur Verwaltung von Speicherressourcen, wie PersistentVolumes und PersistentVolumeClaims. Sie bieten anhaltenden Speicher für Anwendungen.
- Networking Group: Diese Gruppe enthält Objekte zur Netzwerksteuerung, wie Services, Ingresses und NetworkPolicies. Sie ermöglichen die Kommunikation zwischen Anwendungen und bieten Netzwerkrichtlinien.
- Authentication Group: Diese Gruppe beinhaltet Objekte für die Authentifizierung und Autorisierung, wie Secrets und ServiceAccounts. Sie verwalten Zugriffsdaten und Berechtigungen für Anwendungen.

### Struktur Objects und Groups
So sieht es nach der Einführung von Groups aus. Sämtliche Objects wurden gemäss ihren Aufgaben einer passenden Gruppe zugeteilt. Das hat auch Auswirkungen auf die Deklaration im Manifest (z.B. im Deployment.yaml File weiter unten).

| **Object Groups** | 
| :---: | 
| <img src="./images/05_object-groups.png" width="800"/> | 
| Zuteilung der Objects an Groups **core**, **apps**, **batch**, **storage** und **networking** | 


### Group deklaration im Manifest 
Wenn man also in einem Manifest, wie im Beispiel unten, ein Deployment definiert (deployment.yaml), muss die zugehörige Group zwingend in der **ersten Zeile** nach dem Key **apiVersion** angegeben werden. In diesem Fall lautet der Value: **apps/v1**:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: web
spec:
  replicas: 3
  selector:
    matchLabels:
      app: web
  template:
    metadata:
      labels:
        app: web
    spec:
      containers:
      - name: web
        image: tbz-m346-image:tag
        ports:
        - containerPort: 80
```

## Pod
Ein Pod ist das **kleinste konfigurierbare Objekt** in Kubernetes und enthält folgende Informationen:
- Namen des Pods
- Die Container, die darin ausgeführt werden
- Konfiguration für Ressourcen und Netzwerke

| **Aufstellung ohne Coach** | **Aufstellung mit Coach** |
| :---: | :---: |
| <img src="./images/06a_pod.png" width="400"/> | <img src="./images/06b_pod.png" width="400"/> |
| Keine Zuweisung | Verteilt, Organisiert |


Pods werden nicht direkt von Benutzern verwaltet, sondern in der Regel über höhere Abstraktionen wie **Deployments** oder **ReplicaSets**. Diese Abstraktionsschichten ermöglichen das Skalieren, Aktualisieren und Verwalten von Pods, um eine hohe Verfügbarkeit und Skalierbarkeit von Anwendungen zu gewährleisten. Weitere Details zu diesen beiden Objects folgen später.




<br><br><br>



------------------------------------

### Fussball: Mit und ohne Orchestrierung

| **Aufstellung ohne Coach** | **Aufstellung mit Coach** |
| :---: | :---: |
| <img src="./images/03_slide.png" width="400"/> | <img src="./images/04_slide.png" width="400"/> |
| Keine Zuweisung | Verteilt, Organisiert |


### Microservices: Mit und ohne Orchestrierung

| **Container ohne Coach** | **Container mit Coach** |
| :---: | :---: |
| <img src="./images/05.png" width="400"/> | <img src="./images/07.png" width="400"/> |
| Keine Zuweisung | Verteilt, Skaliert, Hochverfügbar |

<br>

## Kubernetes Cluster (Master- und Worker Nodes)

Ein Kubernetes-Cluster besteht normalerweise aus **mehreren Computern** (BareMetal oder Virtuelle Maschinen), die als "**Nodes**" bezeichnet werden. Zu Testzwecken kann Kubernetes aber auch auf nur einem System installiert werden (so machen wir das auch in der TBZ-Cloud).
- **Master Node** ist der zentrale Steuerungspunkt
    - Koordiniert und delegiert alle Aufgaben
    - Verwaltet sämtliche Ressourcen
    - Überwacht dauernd den Status des Clusters 
- **Worker Nodes** sind die ausführenden Einheiten
    - hosten Anwendungen und Services (z.B. Container in Pods)
    - Führen die vom Master Node zugewiesenen Aufgaben aus
    
 Zusammen bilden Master- und Worker Nodes das **Grundgerüst des Kubernetes-Clusters** und ermöglichen die Skalierung, Hochverfügbarkeit und effiziente Verwaltung von Containern. Der Kubernetes Master und die Worker Nodes arbeiten **Hand in Hand**, um Anwendungen in einem Cluster zu verwalten. 

### Beispiel aus dem Alltag
Ein gutes Beispiel dafür ist ein Restaurant. Der Küchenchef ist der Kubernetes Master und die Köche sind die Worker Nodes. Der Küchenchef plant und koordiniert die Gerichte, während die Köche die Anweisungen des Küchenchefs befolgen und die Gerichte zubereiten. Der Küchenchef überwacht den Fortschritt und stellt sicher, dass alles reibungslos abläuft.

Zusammen sorgen der Kubernetes Master und die Worker Nodes dafür, dass Anwendungen effizient bereitgestellt werden, Ressourcen optimal genutzt werden und das gesamte System stabil läuft.

| **Kubernetes Cluster** (3 Master Nodes + 6 Worker Nodes) |
| :---: |
| ![K8s Cluster](./images/08_800p.png) |
| Der Master Node (Control-Plane) erhält das Manifest (.yml-File) via RestAPI und beauftragt die Worker Nodes  |

<br>

### Master Node (Control Plane)

Der Master (Control Plane) von Kubernetes hat folgende Hauptkomponenten:

- **API-Server** : 
    - Bietet eine zentrale Schnittstelle für die Kommunikation und Interaktion mit dem Cluster
    - Ist verantwortlich für Benutzerzugriffe und -rechte (Authentifizierung und Autorisierung).
- **Scheduler**: Verteilt Pods auf Knoten basierend auf Ressourcenanforderungen.
- **Controller Manager**:
    - Überwacht den Cluster-Zustand
    - führt automatische Anpassungen durch (Skalierung von Pods, Replikasets, Deployments und anderen Ressourcen)
    - ist verantwortlich für die Gesundheit sämtlicher Komponenten innerhalb des Clusters
- **Cluster store (etcd)**: 
    - Eine hochverfügbare, verteilte NoSQL-Datenbank
    - Speichert den Cluster-Zustand, einschließlich Konfigurationen und Informationen über Ressourcenobjekte.

| **Kubernetes Master Node** (Control Plane) |
| :---: |
| ![K8s Master](./images/14_800p.png) |
| Sämtliche Komponenten kommunizieren **immer** über den **apiserver{}** |

#### Multi-Master H/A Control Plane
Ein redundanter Kubernetes Master (Control Plane) gewährleistet die hohe Verfügbarkeit des Clusters, da Ausfälle einzelner Komponenten kompensiert werden können.

**Best Practice** <br>
- Es wird empfohlen, eine **ungerade Anzahl von Master-Nodes** zu verwenden, typischerweise **3** oder **5**. Das ermöglicht eine höhere Ausfallsicherheit (falls ein Master ausfällt) und gleichzeitig eine schnelle Entscheidungsfindung bei der Auswahl eines neuen Masters (Leader Election). 
- Bei einer **geraden Anzahl von Master-Nodes** könnte es zu Konflikten kommen, weil eine Abstimmung auch zu einem Patt führen könnte.


| **Sweet Spot: 3 oder 5 Master Nodes** | **Best Practice Beispiel** |
| :---: | :---: |
| <img src="./images/12.png" width="400"/> | <img src="./images/11.png" width="400"/> |
| Zuviele Master-Nodes erschweren die Entscheidung | 1x Master und 2x Active Standby |


<br>

### Worker Nodes

Ein Worker Node in Kubernetes ist für die Ausführung von Anwendungen und Containern verantwortlich. Er spielt eine entscheidende Rolle bei der Bereitstellung von Rechenleistung und Ressourcen für die Anwendungen, die in Kubernetes laufen.

Die Hauptkomponenten im Kubernetes Worker Node sind:

- **Kubelet:** Der Kubelet ist ein Agent, der auf jedem Worker Node läuft und die Kommunikation zwischen dem Node und dem Kubernetes-Master ermöglicht. Er ist für die Verwaltung und Überwachung der Pods auf dem Node zuständig.
- **Container Runtime**: Die Container-Runtime, wie Podman, Docker oder containerd, ist verantwortlich für das Starten, Stoppen und Verwalten von Containern auf dem Worker Node.
- **Kube-Proxy**: Der Kube-Proxy ermöglicht die Netzwerkkommunikation zwischen den Pods auf dem Worker Node und anderen Pods oder Diensten im Cluster. Er leitet den Netzwerkverkehr entsprechend den Kubernetes-Service-Definitionen weiter.
- **CNI-Plugin:** Das CNI (Container Network Interface) Plugin ermöglicht die Konfiguration und Verwaltung des Netzwerks für die Pods auf dem Worker Node. Es sorgt für die Zuweisung von IP-Adressen, das Routing und andere Netzwerkfunktionen.

Zusammen stellen diese Komponenten sicher, dass die Anwendungen in Containern auf dem Worker Node ausgeführt werden, das Netzwerk korrekt konfiguriert ist und die Kommunikation mit dem Kubernetes-Master gewährleistet ist.

| **Worker Node** |
| :---: |
| ![K8s Master](./images/15_800p.png) |
| Sämtliche Komponenten kommunizieren **immer** über den **apiserver{}** |

<br>

## Workflow (Deployment eines Manifests)

Hier ein Beispiel, wie ein sehr einfacher Workflow aussehen könnte. Der folgende Workflow zeigt, wie ein YAML-Manifest für die Definition eines Pods an den Kubernetes-Cluster übergeben wird und wie Kubernetes den Start und die Überwachung der Pods auf den Nodes ermöglicht.

1. Zuerst wird ein YAML-Manifest erstellt, das einen Pod mit dem Namen "web" und 3 Replikas definiert.
2. Das YAML-Manifest wird über eine REST-API an den Kubernetes-Cluster gesendet.
3. Der Kubernetes-Cluster empfängt das Manifest und validiert es.

| 2. Übermittlung an Cluster via REST-API | 3. Speicherung im Cluster Store (etcd, NoSQL-DB) |
| :---: | :---: |
| <img src="./images/24a_400p.png"/> | <img src="./images/24b_400p.png" /> |
| User sendet mit **kubectl** per HTTP das Manifest an den API-Server  | Nach der Validierung wird es im Key/Value Store abgelegt |

4. Der Scheduler im Kubernetes-Master wählt die zur Verfügung stehenden Nodes im Cluster aus, auf denen die Pod-Replikas ausgeführt werden sollen.
5. Die Kubelet-Agenten auf den drei ausgewählten Nodes nehmen den Auftrag entgegen und starten die entsprechenden Pod-Replikas gemäss Vorgabe. In diesem Fall ist das pro Node ein Replika.

| Anweisung an ersten Node| Anweisung an zweiten Node | Anweisung an dritten Node |
| :---: | :---: |:---: |
| <img src="./images/24c.png" width="300"/> | <img src="./images/24d.png" width="300"/> | <img src="./images/24e.png" width="300"/> |
| Erzeuge **Pod 1** von 3 Replikas | Erzeuge **Pod 2** von 3 Replikas | Erzeuge **Pod 3** von 3 Replikas |

6. Die Pods werden auf den Nodes gestartet und der Kube-Proxy ermöglicht die Kommunikation zwischen den Pods und anderen Ressourcen im Cluster.
7. Der Cluster überwacht den Zustand der Pods und stellt sicher, dass die definierte Anzahl von Replikas immer ausgeführt wird.

| **Überwachung in Echtzeit** |
| :---: |
| ![K8s Controller](./images/24f_800p.png) |
| Die Kubelets senden regelmässig den Status ihres Nodes (Watch loop). **Desired State** und **Observed State** müssen übereinstimmen |

<br>

## :memo: Nachweis:
Wenn Du in der Lage bist, Deinem Coach Antworten zu den folgenden und weiteren Fragen zu geben, dann bist Du bereit für den nächsten Schritt. Halte die Antworten entweder im eigenen Repo oder ePortfolio fest. Das ist die einzige Quelle, die Du während dem Fachgespräch benutzen darfst :wink: 

**Architektur**
- [ ] Nenne **zwei** Konkurrenzprodukte von Kubernetes.
- [ ] Was ist die **Kernaufgabe** von Kubernetes?

**Master Node(s)**
- [ ] Wieviele Master Nodes sind gem. Best Practice sinnvoll?
- [ ] Welche Aufgaben übernimmt der Master Node?
- [ ] Wie nennt man den Master Node auch noch?
- [ ] Aus welchen Komponenten besteht der Master Node?
- [ ] Über welche Schnittstelle des Master Nodes kommunizieren **sämtliche** Komponenten?
- [ ] Welche Funktion hat der **Scheduler** im Master Node?
- [ ] Fragen zum **Cluster Store** (etcd, NoSQL-Datenbank): 
    - Weshalb wird hier eine **NoSQL-Datenbank** einer **Relationalen Datenbank** vorgezogen?
    - Es werden hier sogenannte **Key/Value-Pairs** abgelegt. Nenne ein Bespiel
- [ ] Welche Komponente überprüft regelmässig den Status der Worker Nodes?

**Worker Node(s)**
- [ ] Welche Aufgaben übernehmen die Worker Nodes?
- [ ] Aus welchen Komponenten besteht ein Worker Node?
- [ ] Welche Funktion hat der **Kubelet** im Worker Node?
- [ ] Welche Komponente startet und stoppt die Container?
- [ ] Welche Container-Runtime läuft in Kubernetes? (Spoiler: früher war es Docker)
- [ ] Welche Komponente kommuniziert mit dem Scheduler des Master Nodes?

<br><br>

---

> [⇧ **Zurück zum Entrypoint dieses Kapitels** (OCI Images & Container orchestrieren mit Kubernetes)](../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ser-cal/m346-lab)


---