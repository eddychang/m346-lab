# OCI Images & Container orchestrieren mit Kubernetes



## Aufträge

#### 1. Auftrag (Theorie)
- [Kubernetes Cluster Architektur: ](1/README.md)

#### 2. Auftrag
- [Kubernets Objects](2/README.md)

#### 3. Auftrag 
- [Kubectl: Zugriff auf den Kubernetes-Cluster](3/README.md)

#### 4. Auftrag 
- [xxx](3/README.md)


TO BE DEFINED....

#### 4. SOL: Drei AWS Use-Cases (Separates Repository)
- [Entrypoint](https://gitlab.com/ser-cal/aws-ec2-and-container-webserver)
    - [AWS EC2 Webserver mit UserData aufsetzen](https://gitlab.com/ser-cal/aws-ec2-and-container-webserver/-/tree/main/01_EC2-Webserver-UserData)
    - [AWS EC2 Webserver mit User- und MetaData aufsetzen](https://gitlab.com/ser-cal/aws-ec2-and-container-webserver/-/tree/main/02_EC2-Webserver-User-und-Metadata)


---

> [⇧ **Zurück zur Hauptseite**](../README.md)
