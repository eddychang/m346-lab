# 1.4 - Container Orchestrierung mit Docker Swarm (Deklarative Methode)

#### Einstieg

Swarm-Stack ist eine Funktion von Docker, die es ermöglicht, Anwendungen mithilfe des **Docker Swarm-Orchestrierungstools** in einem Cluster von Containern zu erstellen und zu verwalten. Docker Swarm ist ein in Docker integriertes Tool für die Containerorchestrierung, das es ermöglicht, Container **über mehrere Hosts hinweg** zu verteilen und zu skalieren.

Mit Swarm-Stack können Entwickler Docker-Compose-Dateien verwenden, um komplexe Anwendungen zu definieren und in einem Swarm-Cluster bereitzustellen. Docker-Compose ist eine Textdatei im YAML-Format, die die verschiedenen Komponenten einer Anwendung sowie deren Konfiguration und Verbindungen definiert.

Ein Swarm-Stack besteht aus **einer oder mehreren Docker-Compose-Dateien**, die als **Service-Definitionen** bezeichnet werden. Jeder Service repräsentiert eine Komponente der Anwendung, wie zum Beispiel eine Datenbank, einen Webserver oder einen Microservice. Die Services können miteinander kommunizieren und abhängigkeiten haben, was die Erstellung und Verwaltung von komplexen Anwendungen erleichtert.

Der Swarm-Stack-Manager übernimmt die Verwaltung und Bereitstellung der Services in einem Swarm-Cluster. Er verteilt die Services auf die verfügbaren Hosts im Cluster und skaliert sie je nach Bedarf hoch oder runter. Der Manager stellt sicher, dass die Services immer verfügbar sind, indem er sie bei Ausfällen automatisch neu startet und die Last auf die verfügbaren Ressourcen im Cluster verteilt.

Swarm-Stack bietet auch Funktionen für das **Service-Discovery**, **Load-Balancing** und das **Routing** des Datenverkehrs zu den richtigen Services. Es ermöglicht die Verwendung von Netzwerk-Overlays, um die Kommunikation zwischen den Containern in verschiedenen Hosts abzusichern und zu vereinfachen.

Zusammenfassend ermöglicht Swarm-Stack von Docker die einfache Erstellung, Bereitstellung und Verwaltung von komplexen Anwendungen mithilfe von Docker Swarm-Orchestrierungstools. Es erleichtert die Skalierung, Verteilung und Ausfallsicherheit von Anwendungen in einem Cluster von Containern und bietet Funktionen für Service-Discovery, Load-Balancing und das Routing des Datenverkehrs.

**Zusammenfassung :**
Swarm-Stack von Docker ermöglicht die einfache **Erstellung**, **Bereitstellung** und **Verwaltung** von komplexen Anwendungen mithilfe von Docker Swarm-Orchestrierungstools. **Es erleichtert die Skalierung, Verteilung und Ausfallsicherheit von Anwendungen** in einem Cluster von Containern und bietet Funktionen für Service-Discovery, Load-Balancing und das Routing des Datenverkehrs.


## Aufgabenstellung 1.4

#### Ablauf:

**1.** Im Tutorial (ca. 7') lernst Du nach einem kurzen Intro die Anwendung von **Docker stack** kennen. Wir werden anhand eines Beispiels sehen, wie man mit wenigen Manipulationen eine Anwendung skalieren und mittels **integriertem L3-Loadbalancer** verteilen kann.
**2.** Im zweiten Schritt wirst Du dieselbe Anwendung noch ein bisschen nach Vorgaben **customizen** und in **Deinem Gitlab-Registry** dokumentieren.


### 1. Tutorial "Docker-stacks (Part 4)" anschauen

#### Vorgehen:
- Tutorial **in Ruhe anschauen** - Kopfhörer empfohlen, Selbststudium  
- [**Text-File**](./04_Journal-Docker-Bootstrap.txt) welches im Tutorial benutzt wird
- **Wichtige und nutzvolle** Informationen im eigenem Repo dokumentieren (z.B. `docker service create...` Command). Diese können nachher beim 2. Teil (Hands-on Übungen) nützlich sein.


![Video1:](../../images/Video.png) 07:20
[![Tutorial](../../images/docker-part4_200.png)](https://web.microsoftstream.com/video/a62decfb-fa58-41d2-b782-0d024733f6cd?list=studio)


<br>

------

<br>
 
### 2. Questions and Answers

Hier noch ein paar **Fragen** zu Begriffen, **die im Tutorial ausführlich erklärt werden** und die Du am besten noch in Deinem Repo verewigst.


#### Check: Topics, die in der Doku festgehalten sein sollten:

- [x] Erkläre, welcher Eintrag im Config-File **docker-compose.yml** im Vergleich zur letzten Übung dazugekommen ist
- [x] Hat diese Ergänzung einen Einfluss auf die **Performance**? **Ja/Nein** mit Begründung
- [x] Was ist die Aufgabe eines **Loadbalancers**?
- [x] Woran erkennt man, dass die vorangegangene Übung als **Imperativ**- und diese Übung als **Deklarativ** bezeichnet wird?
- [x] Was bedeutet der Begriff **Desired State**
- [x] Was muss im Gegensatz zur letzten Übung hier noch gemacht werden? (Hint: OCI-Image, Clip 2:05)

<br>

------

<br>

### 3. Hands-on: Docker Stacks

Lade zuerst das früher erstellte **OCI-Image** von Gitlab auf deine Testumgebung.


#### Voraussetzungen
- Falls Du dies im letzten Auftrag noch nicht gemacht hast, solltest Du zuerst das Repository [**Docker-bootstrap**](https://gitlab.com/ser-cal/docker-bootstrap/-/tree/main/) klonen und dann rein"hüpfen". 
- Für diese Übung wechselst Du am besten gleich in das
Unterverzeichnis [**swarm-stack**](https://gitlab.com/ser-cal/docker-bootstrap/-/tree/main/swarm-stack). 


<br>

----

<br>

#### Nachweis:
Führe Deinem Coach anhand einer Live-Demo folgende Topics vor: 
- [ ] Beweisen, dass **vor** dem Start der Vorführung **kein** Dienst läuft:  `<docker stack ls`
- [ ] Starte mit dem vorher heruntergeladenen OCI-Image einen Service, der `m346-counter` heisst und **3 Replicas** besitzt 
- [ ] Gebe den Befehl `docker stack services m346-counter` ein und überprüfe, ob die Bedingungen erfüllt sind
- [ ] Zeige **sämtliche ID's** der Container _`(Kommando selber recherchieren)`_
- [ ] Erstelle **5** weitere Replicas (Total: 8 Container) _`(Kommando selber recherchieren)`_
- [ ] Entferne **4** der alten Replicas (Total 4 Container) _`(Kommando selber recherchieren)`_
- [ ] Beweise, dass der Loadbalancer der Applikation funktioniert _`(Demonstration / Vorher vorbereiten - Quelle: Tutorial)`_
- [ ] Erkläre mit Fachbegriffen, welchen Einfluss das auf die Verfügbarkeit / Performance der Applikation hat
- [ ] Clean-up: `docker stack rm m346-counter`
- [ ] Beweis, dass gelöscht: `docker stack ls`
 <br>

Der Auftrag ist abgeschlossen, sobald **sämtliche** Bedingungen erfüllt sind. 

<br>

---

> [⇧ **Zurück zum Entrypoint dieses Kapitels** (OCI-Images & Container administrieren mit Docker)](../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ser-cal/m346-lab)

---