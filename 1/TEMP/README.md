[10]: https://git-scm.com/downloads


Container
====

### Ablauf

- [Einstiegspräsentation](docs/02_Container_Einfuehrung.pdf)
- 4 Hands-on Übungen mit separatem Gitlab Repository und unterstützenden Tutorials

Um den Einstieg in dieses umfangreiche Thema zu vereinfachen und vertiefen, haben wir ein separates [**Gitlab Repository**](https://gitlab.com/ser-cal/docker-bootstrap) mit verschiedenen Hands-on Übungen erstellt. 

Für diese Übungen wird die TBZ-Cloud eingesetzt. Es ist aber auch möglich, eine eigene Umgebung aufzusetzen, um die Labs nachzubauen. Die Teilthemen sind so aufgebaut, dass sie am besten der Reihe nach durchgearbeitet werden. Zur Vereinfachung kann [**dieses Textdokument**](docs/01_Journal-Docker-Bootstrap.txt) mit den zugehörigen Kommandos, welche während den Tutorials verwendet werden, genutzt werden.

Dieses beinhaltet folgende **vier Teilthemen**

1. Docker Image und Container (Part 1)
2. Docker-Compose (Part 2)       
3. Docker Swarm (Part 3)
4. Docker-stacks (Part4)


**1.** Docker Image und Container aufsetzen (Part1)

![Video1:](../images/Video.png) 14:07
[![Tutorial](../images/docker-part1_200.png)](https://web.microsoftstream.com/video/b4143f73-2f96-446d-ba66-c44e8ddcb270)

**2.** Docker-Compose (Part 2)

![Video2:](../images/Video.png) 07:41
[![Tutorial](../images/docker-part2_200.png)](https://web.microsoftstream.com/video/c376d6ef-7711-458e-bccd-847abe95468c) 


**3.** Docker Swarm (Part 3)

![Video3:](../images/Video.png) 10:11
[![Tutorial](../images/docker-part3_200.png)](https://web.microsoftstream.com/video/5da624e2-6a09-45f3-922b-a4ac5e9a59f6)


**4.** Docker-stacks (Part4)

![Video4:](../images/Video.png) 07:20
[![Tutorial](../images/docker-part4_200.png)](https://web.microsoftstream.com/video/a62decfb-fa58-41d2-b782-0d024733f6cd)


<br>

### Weitere Tutorials
Folgend noch einige gute Tutorials von **Marco Berger** zu den gleichen Themen:

![Video5:](../images/Video.png) 18:23
[**Docker Images**](https://web.microsoftstream.com/video/df4e25df-b998-4c19-9263-17d3f32a6015)

![Video6:](../images/Video.png) 28:08
[**Docker run**](https://web.microsoftstream.com/video/83fcd7b5-c36b-4ca8-8433-73d7aff4fb19)

![Video7:](../images/Video.png) 20:47
[**Docker build**](https://web.microsoftstream.com/video/b05df9cf-f3c9-4f52-8df4-deafbefbc2b7)

![Video8:](../images/Video.png) 05:21
[**Docker Network**](https://web.microsoftstream.com/video/96056452-99dd-49ee-9233-09fdf13583cf)

![Video9:](../images/Video.png) 22:14
[**Docker Compose**](https://web.microsoftstream.com/video/c8605a61-35c6-4380-9fb9-387733085b44)

---

![Video10:](../images/Video.png) 22:14
[**Kubernetes in 5 Min.**](https://web.microsoftstream.com/video/ae1784a6-c42b-4f65-877f-99729bfbc6d9)


---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ser-cal/m346-lab)

---
