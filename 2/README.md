# Cloud-init, OCI Images & Container in der AWS Cloud



## Aufträge

#### 1. Auftrag
- [eMail-Alias & AWS Free Tier Account erstellen](1/README.md)

#### 2. Auftrag 
- [AWS Alias und Billing Alarm erstellen](2/README.md)

#### 3. Auftrag 
- [AWS IAM User-Account einrichten](3/README.md)

#### 4. SOL: Drei AWS Use-Cases (Separates Repository)
- [Entrypoint](https://gitlab.com/ser-cal/aws-ec2-and-container-webserver)
    - [AWS EC2 Webserver mit UserData aufsetzen](https://gitlab.com/ser-cal/aws-ec2-and-container-webserver/-/tree/main/01_EC2-Webserver-UserData)
    - [AWS EC2 Webserver mit User- und MetaData aufsetzen](https://gitlab.com/ser-cal/aws-ec2-and-container-webserver/-/tree/main/02_EC2-Webserver-User-und-Metadata)
    - [AWS ECS (Elastic Container Service) Fargate](https://gitlab.com/ser-cal/aws-ec2-and-container-webserver/-/tree/main/03_ECS-Webserver-NGINX)
<br>

---

> [⇧ **Zurück zur Hauptseite**](../README.md)
