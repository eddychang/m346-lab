# AWS IAM User-Account einrichten

## Aufgabenstellung 2.3

Durch die Verwendung von IAM-Benutzern können Unternehmen die **Sicherheit erhöhen**, den Zugriff auf ihre AWS-Ressourcen **effektiv verwalten** und die Risiken von Datenlecks und unbefugtem Zugriff minimieren. IAM bietet eine solide Grundlage für die **sichere und effiziente Nutzung** der Cloud-Infrastruktur von AWS.

<br>

----------

#### Ablauf:

- **1.** Zuerst wird anhand eines Tutorials gezeigt, welche Unterschiede zwischen dem **Root-Account** und einem **IAM-User** bestehen und wie man einen **IAM-user** erstellt, diesen einer **Gruppe** zuweist und dieser Gruppe die **Administratoren-Policy** zuteilt. Damit stellen wir sicher, dass unser **IAM-User** genügend Rechte hat für die bevorstehenden Übungen.
- **2.** Im zweiten Schritt führst Du dieselben Schritte in Deinem AWS-Account durch. Du erstellst in **Deinem AWS-Account** einen neuen **IAM-User**, weist diesen der Gruppe **Administratoren** zu und berechtigst diese Gruppe mit der Policy **AdministratorAccess**.


### 1. Tutorial anschauen: AWS IAM User-Account einrichten

#### Vorgehen:
- Tutorial **in Ruhe anschauen** - Kopfhörer empfohlen, Selbststudium. 
- **Wichtige und nutzvolle** Informationen im eigenem Repo dokumentieren (z.B. Unterschied zwischen Root-Account und IAM-User, Username, Gruppen- und Policy-Zuweisung).


![Video1:](../../images/Video.png) 5:10
[![Tutorial](../../images/aws-user.png)](https://web.microsoftstream.com/video/58b96b9e-2d8f-485d-8339-6507e3317ee3?list=studio)


<br>

------

<br>

### 2. Hands-on: AWS IAM User-Account einrichten

- Erstelle in Deinem AWS-Account einen **neuen IAM-User** (Name frei wählbar).
- Erstelle eine neue Gruppe mit dem Namen **Administratoren**.
- Weise der Gruppe **Administratoren** die Policy **AdministratorAccess** zu.
- Weise den neuen **IAM-User** der Gruppe **Administratoren** zu.


<br>

----


#### Nachweis:
Führe Deinem Coach anhand einer Live-Demo folgende Topics vor: 
- [ ] Du loggst Dich mit dem **neuen IAM-User** ein und nutzt dabei den früher erstellten **Konto-Alias** (anstatt mit der 12-stelligen Account ID)
- [ ] Zeige, dass Dein IAM-User in der Gruppe **Administratoren** ist und diese die Policy **AdministratorAccess** besitzt
- [ ] Öffne die Policy **AdministratorAccess**, erkläre die Keys **Effect**, **Action** und **Resource** und deren **Values**
- [ ] Zeige Dein nachgeführtes Repo (alle wichtigen Schritte sind darin abgebildet und beschrieben)
<br>


---

> [⇧ **Zurück zum Entrypoint dieses Kapitels** (Cloud-init, OCI Images & Container in der AWS Cloud)](../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ser-cal/m346-lab)

---

---