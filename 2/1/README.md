# AWS Account erstellen (mit Google-Alias)

## Aufgabenstellung 2.1

### Einführung:

Um den AWS Free-Tier Account auch in den kommenden Jahren weiterhin kostenlos nutzen zu können, empfiehlt es sich, einen Email-Alias zu verwenden. Dieser ermöglicht es, jährlich einen neuen AWS Free-Tier Account zu erstellen, ohne dass eine neue E-Mail-Adresse erforderlich ist. In diesem Zusammenhang werden wir zuerst einen solchen Email-Alias erstellen.

### Ablauf:

**1.** Erstelle einen Google eMail-Alias. Im Gitlab-Repository [CAL_AWS-Account](https://gitlab.com/ser-cal/cal_aws-account) gibt es zwei Anleitungen. 
- Schnellanleitung (ohne Bilder und ausführlichen Erklärungen)
- Bild für Bild Anleitung (ausführlich)

**2.** Erstelle mit diesem eMail-Alias einen **AWS Free-Tier Account**

<br>

----

### 1. Google eMail Alias erstellen

#### Vorgehen:
- Erstellen eines neuen Google Email-Alias für die Verwendung mit AWS Free-Tier Accounts.
- Überprüfen der Funktionalität des erstellten Email-Alias, indem eine Testnachricht gesendet und empfangen wird.



- Repository **in Ruhe durcharbeiten** - Selbststudium  
- **Screenshots, wichtige und nutzvolle** Informationen im eigenem Repo dokumentieren (z.B. eMail-Alias). 

- ...weiter zum Repository: **Klick** auf's **Book-Icon** unten:

     [![Repository](../../images/Buch.jpg)](https://gitlab.com/ser-cal/cal_aws-account)


<br>

------

### 2. AWS Free Tier Account erstellen

Der **AWS Free-Tier Account** bietet eine attraktive und  risikofreie Möglichkeit, die AWS-Plattform zu erkunden und  verschiedene AWS-Dienste kennenzulernen. Du wirst mit verschiedenen Hands-on Übungen Begriffe wie Skalierbarkeit, Verfügbarkeit, Flexibilität und Integration praktisch kennenlernen, ohne finanzielle Barrieren zu haben.

#### Vorgehen:
- Erstellung eines AWS Free-Tier Accounts unter Verwendung der zuvor erstellten Google eMail-Adresse.
- Bestätigen der AWS-Kontoerstellung über den Link in der Bestätigungs-eMail, die an den Google eMail-Alias gesendet wird.
- Überprüfen der erfolgreichen Erstellung des AWS Free-Tier Accounts, indem auf das AWS-Dashboard zugegriffen wird und die kostenlosen Services überprüft werden.
- Dokumentation des Vorgehens, einschließlich der erstellten Google Email-Adresse und des AWS Free-Tier Account-Status.

<br>

----


#### Nachweis:
Führe Deinem Coach anhand einer Live-Demo folgende Topics vor: 
- [ ] Du hast einen persönlichen eMail-Alias erstellt 
- [ ] Du kannst mit diesem eMail-Alias als Root-User auf AWS einloggen
- [ ] Zeige Dein nachgeführtes Repo (alle wichtigen Schritte sind darin abgebildet und beschrieben)
<br>


---

> [⇧ **Zurück zum Entrypoint dieses Kapitels** (Cloud-init, OCI Images & Container in der AWS Cloud)](../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ser-cal/m346-lab)

---