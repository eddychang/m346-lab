[10]: https://github.com
[20]: https://web.microsoftstream.com/video/663c03fd-5562-42f0-855f-fc98e7769c3e


![M346-Banner](images/01_banner.png)


---

# M346 LAB

In diesem Repository sind diverse Hands-on Challenges zu meistern. 
Diese sind wie folgt aufgebaut:
- Theorie: Diese wird Anhand eines Tutorials praxisnah vorgeführt. Es empfiehlt sich dieses gleich selber nachzubauen.
- Praxis: Du erhältst einen Arbeitsauftrag (Bezug zur Theorie oben). Führe diesen selbständig aus und okumentiere das Vorgehen.

Viel Erfolg und viel Spass.


[TOC]

<br>

## Challenges

### OCI Images & Container Administration mit Docker 
* [**Entrypoint**](1/README.md) (_Key aspect: **Docker**_)

    - ##### [1. Auftrag](1/1/README.md) - OCI-Image mit **Docker** erstellen, in Registry ablegen, deployen & Container Administration 

    - ##### [2. Auftrag](1/2/README.md) - Multicontainer-App mit **docker-compose** deklarativ deployen und administrieren

    - ##### [3. Auftrag](1/3/README.md) - Container Orchestrierung mit **Docker Swarm** (Imperative Methode)

    - ##### [4. Auftrag](1/4/README.md) - Container Orchestrierung mit Docker Stack (Deklarative Methode)


---

### Cloud-init, OCI Images & Container in der AWS Cloud
* [**Entrypoint**](2/README.md) (_Key aspect: **AWS**_)

    - ##### [1. Auftrag](2/1/README.md) - eMail-Alias & AWS Free Tier Account erstellen 

    - ##### [2. Auftrag](2/2/README.md) - AWS Alias und Billing Alarm erstellen

    - ##### [3. Auftrag](2/3/README.md) - AWS IAM User-Account einrichten

    - ##### [4. Auftrag](https://gitlab.com/ser-cal/aws-ec2-and-container-webserver) - SOL: Drei AWS Use-Cases (Separates Repository)

--- 

### OCI Images & Container Administration mit Podman
* [**Hands-on Auftrag**](3/README.md) (_Key aspect: Mit **Podman** zwei OCI-Images für die Container Orchestrierung vorbereiten_)

--- 

### OCI Images & Container orchestrieren mit Kubernetes
* [**Entrypoint**](./4/README.md) (_Key aspect: **Kubernetes**_)

<br>


- - -

<br>

- Autor: Marcello Calisto
- Mail: marcello.calisto@tbz.ch
